var backColor = '2b1e32';
var sp = '0_3';//start point
var asp = sp.split('_');//array of start point
var rownum = 20;
var linenum = 10;
var moveStop = 0;//check if next brick is needed.
var failMove = 0;//check if the move fails.(for l and r)
var level = 1;//start level;
var speed = 1000/level;
var dir;//short for direction.
var status = 0;
var prestatus = 3;
var dead = 0;
var cleanedbrick = 0;
var score = 0;
var pause = 0;


function buildBox(){
    for(r=1;r<=rownum;r++){//row
        for(l=1;l<=linenum;l++){
            $('#box').append("<div id=\""+r+"_"+l+"\" class=\"sbox\"></div>");
        };
        $('#box').append("<div class=\"clear\">")
    };
    $('.sbox').css('backgroundColor',backColor);
    $('.sbox').attr('value','0');
}

function brick(shape,color){
    this.shape = shape;
    this.color = color;
}//build a brick class

brick.prototype={
    draw:function(position){
        $('#'+position).css('backgroundColor',this.color);
        $('#'+position).attr('value','1');
    },//draw one div;
    drawbox:function(){
        for(i=0;i<this.sbrick.length;i++){
            this.draw(this.sbrick[i]);
        }
    },//draw a full brick
    clean:function(position){
        $('#'+position).css('backgroundColor',backColor);
        $('#'+position).attr('value','0');
    },//delete a div;
    cleanbox:function(){
        for(i=0;i<this.sbrick.length;i++){
            this.clean(this.sbrick[i]);
        }  
    },//delete a full brick
    check:function(){
        nextSbrick = this.nextSbrick;
        this.predict();//set the value of nextSBrick;
        skip = this.skip;
        for(i=0;i<this.sbrick.length;i++){
            for(j=0;j<this.sbrick.length;j++){
                if(nextSbrick[i]==this.sbrick[j]) skip[i] = 1;
            };
            if(skip[i]!=1){
                if($('#'+nextSbrick[i]).attr('value')==1||nextSbrick[i].split('_')[0]==(rownum+1)||nextSbrick[i].split('_')[1]==(linenum+1)||nextSbrick[i].split('_')[1]==0){
                    if(dir == 'd') {
                        moveStop = 1;
                    };
                    if(dir == 'l'||dir == 'r'||dir == 'u'){failMove = 1;};
                }
            }
        }
    },//edge check function
    predict:function(){
        if(dir == 'd'||dir == 'r'||dir == 'l'){
            if(dir == 'd'){x1=1;y1=0};
            if(dir == 'r'){y1=1;x1=0;};
            if(dir == 'l'){y1=-1;x1=0};
            for(i=0;i<this.sbrick.length;i++){
                nextSbrick[i]=Array(this.sbrick[i].split('_')[0]-0+x1+"",this.sbrick[i].split('_')[1]-0+y1+"").join('_');
            }
        }
        if(dir == 'u'){
            this.rotate(this.nextSbrick,status);
            status = prestatus;
            prestatus = (status -1+4)%4;
        }
    },

    build:function(obj){},
    move:function(direction){
        if(!dead){
            dir = direction;
            this.check();
            if(moveStop==0){
                if(failMove == 0){
                    this.cleanbox();
                    if(dir == 'd'||dir == 'r'||dir == 'l'){
                        if(dir == 'd'){x1=1;y1=0};
                        if(dir == 'r'){y1=1;x1=0};
                        if(dir == 'l'){y1=-1;x1=0};
                        this.x = this.x -0+ x1 +"";
                        this.y = this.y -0+ y1 +"";
                        this.rotate(nowBrick.sbrick,prestatus)
                    };
                    if(dir=='u'){
                        this.rotate(nowBrick.sbrick,status)
                    }
                    this.drawbox();
                }
                else if(failMove == 1){failMove = 0};
            };
            if(moveStop==1){
                this.checkdelete();
                this.buildNext();
            }
        }
    },
    drop:function(){
        if(!dead){
            while(moveStop==0){
                this.move('d');
                this.check();
            };
        }
    },
    buildNext:function(){
        nowBrick.sbrick.splice(0,10);//clear the array of sbrick
        this.x = parseInt(asp[0])+"";
        this.y = parseInt(asp[1])+"";
        nowBrick = preBricks[0];
        refreshPre();
        nowBrick.checkdead();
        nowBrick.build(nowBrick.sbrick);
        nowBrick.drawbox();
        moveStop = 0;//reset moveStop
        status = 0;
        prestatus = 3;
    },//build the next brick.
    checkdead:function(){
        nextSbrick = this.nextSbrick;
        this.build(nextSbrick);
        for(x in nextSbrick){
            if($('#'+nextSbrick[x]).attr('value')==1){
                popout('You\'re Dead!')
                $('#restart').show();
                clearInterval(dropinterval);
                dead = 1;
            }
        }
    },
    sbrick:Array(),//to storage the position of each brick
    checkdelete:function(){
        var valuesum = 0;
        var combocleaned = 0;
        for(k=rownum;k>0;k--){
            for(i=0;i<linenum;i++){valuesum+=parseInt($('#'+k+'_'+(i+1)).attr('value'))};
            if(valuesum == linenum){
                //for(i=0;i<linenum;i++){
                //    $('#'+k+'_'+(i+1)).css('backgroundColor',backColor);
                //    $('#'+k+'_'+(i+1)).attr('value','0');
                //}

                for(i=k;i>0;i--){
                    for(j=0;j<linenum;j++){
                        $('#'+i+'_'+(j+1)).attr('value',$('#'+(i-1)+'_'+(j+1)).attr('value'));
                        $('#'+i+'_'+(j+1)).css('backgroundColor',$('#'+(i-1)+'_'+(j+1)).css('backgroundColor'));
                    }
                }
                k++;
                cleanedbrick++;
                combocleaned++;
                $('#cleanednum').html(cleanedbrick);
                if(cleanedbrick%8==0){//level up after 8 line has been cleaned.
                    levelup();
                }
            }
            valuesum = 0;
        }
        if(combocleaned==4){
            score+=100;
        }
        if(combocleaned==3){
            score+=70;
        }
        if(combocleaned==2){
            score+=30;
        }
        if(combocleaned==1){
            score+=10;
        }
        $('#score').html(score);
        combocleaned = 0;//clear the combo
    },
    //start of define locale variables
    x:parseInt(asp[0])+"",
    y:parseInt(asp[1])+"",//x,y point of the brick.
    nextSbrick:Array(),//to predict position if the move succeed.
    skip:Array(),//array of each brick to check if it would skip.
}

var hBrick = new brick('h','#01e139');
hBrick.build = function(obj){
    for(i=0;i<4;i++){
        x = this.x -0 + i +"";
        y = this.y -0 + "";
        obj[i] = Array(x,y).join('_');
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};
}
hBrick.rotate = function(obj,instatus){
    if(instatus==0){
        obj[0] = Array(this.x-0+2+"",this.y-2+"").join('_');
        obj[1] = Array(this.x-0+2+"",this.y-1+"").join('_');
        obj[2] = Array(this.x-0+2+"",this.y-0+"").join('_');
        obj[3] = Array(this.x-0+2+"",this.y-0+1+"").join('_');
        prestatus=0;
        status=1;
    }
    else if(instatus==1){
        obj[0] = Array(this.x-0+0+"",this.y-1+"").join('_');
        obj[1] = Array(this.x-0+1+"",this.y-1+"").join('_');
        obj[2] = Array(this.x-0+2+"",this.y-1+"").join('_');
        obj[3] = Array(this.x-0+3+"",this.y-1+"").join('_');
        prestatus=1;
        status=2;
    }
    else if(instatus==2){
        obj[0] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+1+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+2+"").join('_');
        prestatus=2;
        status=3;
    }
    else if(instatus==3){
        for(i=0;i<4;i++){
            x = this.x -0 + i +"";
            y = this.y -0 + "";
            obj[i] = Array(x,y).join('_');
        };
        prestatus=3;
        status=0;
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}

}
var sBrick = new brick('s','#a154e2');
sBrick.build = function(obj){
    obj[0] = Array(this.x,this.y).join('_');
    obj[1] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
    obj[2] = Array(this.x-0+1+"",this.y).join('_');
    obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};
}
sBrick.rotate = function(obj,instatus){
    if(instatus==0){
        obj[0] = Array(this.x,this.y).join('_');
        obj[1] = Array(this.x-0+2+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y).join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=0;
        status=1;
    }
    else if(instatus==1){
        obj[0] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+2+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y).join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=1;
        status=2;
    }
    else if(instatus==2){
        obj[0] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+2+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y).join('_');
        obj[3] = Array(this.x-0+"",this.y-0+"").join('_');
        prestatus=2;
        status=3;
    }
    else if(instatus==3){
        obj[0] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y).join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=3;
        status=0;
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}
}
var l1Brick = new brick('l1','#005aec');
l1Brick.build = function(obj){
    obj[0] = Array(this.x-0-1+"",this.y-0-1+"").join('_');
    obj[1] = Array(this.x-0+0+"",this.y-0-1+"").join('_');
    obj[2] = Array(this.x-0+0+"",this.y-0+0+"").join('_');
    obj[3] = Array(this.x-0+0+"",this.y-0+1+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};
}
l1Brick.rotate = function(obj,instatus){
    if(instatus==0){
        obj[0] = Array(this.x-0+1+"",this.y-1+"").join('_');
        obj[1] = Array(this.x-0+1+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[3] = Array(this.x-0-1+"",this.y-0+"").join('_');
        prestatus=0;
        status=1;
    }
    else if(instatus==1){
        obj[0] = Array(this.x-0+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0+1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=1;
        status=2;
    }
    else if(instatus==2){
        obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0-1+"",this.y-0-1+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        prestatus=2;
        status=3;
    }
    else if(instatus==3){
        obj[0] = Array(this.x-0-1+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+0+"",this.y-0-1+"").join('_');
        obj[2] = Array(this.x-0+0+"",this.y-0+0+"").join('_');
        obj[3] = Array(this.x-0+0+"",this.y-0+1+"").join('_');
        prestatus=3;
        status=0;
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}

}

var l2Brick = new brick('l2','#ff7e02');
l2Brick.build = function(obj){
    obj[0] = Array(this.x-0-1+"",this.y-0+1+"").join('_');
    obj[1] = Array(this.x-0+0+"",this.y-0-1+"").join('_');
    obj[2] = Array(this.x-0+0+"",this.y-0+0+"").join('_');
    obj[3] = Array(this.x-0+0+"",this.y-0+1+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};   
}
l2Brick.rotate = function(obj,instatus){
    if(instatus==0){
        obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0-1+"",this.y-0+1+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0+1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=0;
        status=1;
    }
    else if(instatus==1){
        obj[0] = Array(this.x-0+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0+1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        prestatus=1;
        status=2;
    }
    else if(instatus==2){
        obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y-0+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=2;
        status=3;
    }
    else if(instatus==3){
        obj[0] = Array(this.x-0-1+"",this.y-0+1+"").join('_');
        obj[1] = Array(this.x-0+0+"",this.y-0-1+"").join('_');
        obj[2] = Array(this.x-0+0+"",this.y-0+0+"").join('_');
        obj[3] = Array(this.x-0+0+"",this.y-0+1+"").join('_');
        prestatus=3;
        status=0;
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}
}
var z1Brick = new brick('z1','#e83c26');
z1Brick.build = function(obj){
    obj[0] = Array(this.x-0+"",this.y-0-1+"").join('_');
    obj[1] = Array(this.x-0+0+"",this.y-0+"").join('_');
    obj[2] = Array(this.x-0+1+"",this.y-0+"").join('_');
    obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};   
}
z1Brick.rotate = function(obj,instatus){
    if(instatus==0||instatus==2){
        obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0-1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0-1+"").join('_');
        prestatus=0;
        status=1;
    }
    else if(instatus==1||instatus==3){
        obj[0] = Array(this.x-0+"",this.y-0-1+"").join('_');
        obj[1] = Array(this.x-0+0+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+1+"",this.y-0+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=1;
        status=2;
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}
}
var z2Brick = new brick('z1','#3bd249');
z2Brick.build = function(obj){
    obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
    obj[1] = Array(this.x-0-1+"",this.y-0+1+"").join('_');
    obj[2] = Array(this.x-0+"",this.y-0-1+"").join('_');
    obj[3] = Array(this.x-0+"",this.y-0+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};   
}
z2Brick.rotate = function(obj,instatus){
    if(instatus==0||instatus==2){
        obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0+"",this.y-0+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0+1+"").join('_');
        obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
        prestatus=0;
        status=1;
    }
    else if(instatus==1||instatus==3){
        obj[0] = Array(this.x-0-1+"",this.y-0+"").join('_');
        obj[1] = Array(this.x-0-1+"",this.y-0+1+"").join('_');
        obj[2] = Array(this.x-0+"",this.y-0-1+"").join('_');
        obj[3] = Array(this.x-0+"",this.y-0+"").join('_');
        prestatus=1;
        status=2;
    }
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}
}
var oBrick = new brick('o','#f9d155')
oBrick.build = function(obj){
    obj[0] = Array(this.x-0+"",this.y-0+"").join('_');
    obj[1] = Array(this.x-0+1+"",this.y-0+"").join('_');
    obj[2] = Array(this.x-0+0+"",this.y-0+1+"").join('_');
    obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0};   

}
oBrick.rotate = function(obj,instatus){
    obj[0] = Array(this.x-0+"",this.y-0+"").join('_');
    obj[1] = Array(this.x-0+1+"",this.y-0+"").join('_');
    obj[2] = Array(this.x-0+0+"",this.y-0+1+"").join('_');
    obj[3] = Array(this.x-0+1+"",this.y-0+1+"").join('_');
    for(i=0;i<this.sbrick.length;i++){skip[i]=0}
    prestatus = 3;
    status = 0;
}
var bricks = new Array();
bricks[0] = hBrick;
bricks[1] = sBrick;
bricks[2] = l1Brick;
bricks[3] = l2Brick;
bricks[4] = oBrick;
bricks[5] = z1Brick;
bricks[6] = z2Brick;
var nowBrick;
var preBricks = new Array();
function refreshPre(){
    if(!preBricks[0]){
        preBricks[0]=bricks[Math.floor(Math.random()*(bricks.length)+1)-1];
    }
    else preBricks[0]=preBricks[1];

    if(!preBricks[1]){
        preBricks[1]=bricks[Math.floor(Math.random()*(bricks.length)+1)-1];
    }
    else preBricks[1]=preBricks[2];

    preBricks[2]=bricks[Math.floor(Math.random()*(bricks.length)+1)-1];
    $('#pre0').html(preBricks[0].shape);
    $('#pre1').html(preBricks[1].shape);
    $('#pre2').html(preBricks[2].shape);
}
var dropinterval;
nowBrick = bricks[Math.floor(Math.random()*(bricks.length)+1)-1];
//= setInterval(function(){nowBrick.move('d')},speed);//autuomatically drop

var k = new Kibo();
k.down(['space'],function(){if(!pause) nowBrick.drop();});
k.down(['up'],function(){if(!pause) nowBrick.move('u');});
k.down(['left'],function(){if(!pause) nowBrick.move('l');});
k.down(['right'],function(){if(!pause) nowBrick.move('r');});
k.down(['down'],function(){if(!pause) nowBrick.move('d');});//key control using Kibo()
k.down(['esc'],function(){
    if(pause){
        popout('continue');
        clearpopout();
        dropinterval = setInterval(function(){nowBrick.move('d')},speed);//autuomatically drop
        pause = 0;
    }
    else if(!pause&&dead==0){
        popout('pause');
        clearInterval(dropinterval);
        pause = 1 };
});

function popout(content){
    var font_size = 390/(content.length);
    $('#popout').css('fontSize',font_size);
    $('#popout').html(content);
    $('#popout').css('margin-top',-0.5*parseInt($('#popout').css('height')));
    $('#popout').animate({opacity:'1'},300);
}
function clearpopout(){
    setTimeout(function(){
        $('#popout').animate({opacity:'0'},500);
        $('#popout').html('');
    },500)
}
var countDownNum = 3
function countDown(){
    dead = 1;
    countdown = setInterval(function(){
        if(countDownNum==0){
            clearInterval(countdown);
            dead = 0;
            dropinterval = setInterval(function(){nowBrick.move('d')},speed);//autuomatically drop
            return;
        };
        popout(''+countDownNum);
        clearpopout();
        countDownNum--;
    }
    ,1000)
    countDownNum=3;//reset
}
function levelup(){
    level++;
    clearInterval(dropinterval);
    speed = 1000/level;
    dropinterval = setInterval(function(){nowBrick.move('d')},speed);//autuomatically drop
    $('#levelnum').html(level);
    popout('level '+level);
    clearpopout();
}
countDown();
function restart(){
    $('#restart').hide();
    clearInterval(dropinterval);
    $('#box').html('<div id=\"popout\" style=\"opacity:0;\"></div>');
    speed=1000/level;
    level=1;
    score=0;
    buildBox();
    $('#levelnum').html(level);
    $('#score').html(score);
    preBricks.splice(0,3);//clear the array of sbrick
    refreshPre();
    nowBrick = bricks[Math.floor(Math.random()*(bricks.length)+1)-1];
    countDown();
}
$(document).ready(function(){
    $('#levelnum').html(level);
    $('#score').html(score);
    refreshPre();
})//initialize the level and score.
